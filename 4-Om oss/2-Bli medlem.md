## Bli medlem

Nøklevann ro- og padleklubb (NRPK) er et idrettslag tilknyttet Norges idrettsforbund gjennom Norges Padleforbund og Norges Roforbund. Grunntilbudet til medlemmer er muligheten til å benytte klubbens båter og utstyr på Nøklevann i åpningstiden.

### Slik blir du medlem i NRPK

**Alle** som ønsker å bli medlem i NRPK må først gjennomgå klubbens obligatoriske introduksjonskurs. **Har du tatt kurs i Padleforbundets våttkortstige eller padlekurs andre steder / klubber, må du likevel ta NRPKs introkurs på Nøklevann for å bli medlem i NRPK.** Kurset varer i tre timer og koster kr. 300,-.  For å kunne delta på kurset må man kunne svømme. Siden klubbreglene tillater barn over 12 år å bruke klubbens båter på egen hånd dersom de har foreldrenes samtykke, må barn som skal bli medlem i klubben ta introkurset f.o.m. det kalenderåret de fyller 12 år.
**NB: det har ingen hensikt å søke om medlemskap via MinIdrett før du har gjennomført introduksjonskurs.**

### Hensikten med NRPKs introkurs:
• Øke sikkerheten ved padling og roing på Nøklevann.
• Gi grunnleggende kunnskaper om klubben og klubbens materiell slik at nye medlemmer får større glede og nytte av medlemskapet, samt sikre ryddig og god drift av klubben.

Under kurset skal alle deltagere prøve en liten tur i kano eller kajakk. **Alle skal velte, så derfor må håndkle og tøyskift medbringes.** Det er et viktig moment i forbindelse med sikkerhet ved padling at alle har erfart dette. På kurset vil velting gjennomføres enten to og to under oppsyn av instruktør, eller på grunt vann med en instruktør som står ved siden av båten. Velting/kantring i seg selv er ufarlig, du glir uten problem ut av de båtene vi bruker på kurset. Kurset (inklusiv skifting til tørt tøy) gjennomføres utendørs, uansett vær. NB! Kurset er tilpasset aktiviteten på Nøklevann, og er ikke tilstrekkelig til f.eks. å begynne med hav- eller elvepadling.

Les gjerne mer om **introkurs**  som også ligger under Aktiviteter / Kurs.
Klikk på «**Introkurs (for å bli medlem i NRPK)»**

### Påmelding til introkurs:
Først må du melde deg på [**ventelisten**](http://eepurl.com/bo0_nv) for å få informasjon om introkurs.
NB! Les kvitteringen som kommer opp når du har lagt inn navn og e-postadresse på ventelisten.  Trykk gjerne på linken for å legge ventelisten til din adressebok, for å hindre at senere invitasjoner til påmelding havner i SPAM-filteret.

Det kommer ikke noen e-post med én gang du har meldt deg på ventelisten, men først når nye introkurs settes opp.  Fra ventelisten sender vi ut i tur og orden invitasjon til å melde seg på konkrete kursdatoer etter hvert som disse blir satt opp. Du klikker på link til en aktuell kursdato og fyller ut påmeldingsskjemaet.  For å få bekreftet din plass på introduksjonskurset, må du fullføre påmeldingen og betalingen (kr. 300,- i 2018) må registreres. Den er i utgangspunktet ikke refunderbar, med unntak av sykmelding m/ legeattest.

Følg med på e-posten du la inn på ventelisten. Les gjerne litt generell info om introkurs på våre nettsider under Aktiviteter/Kurs*. Det kan hende at det kommer litt info på Facebook også…
*Tre klikk:  Aktiviteter (til høyre for klubblogoen), så Kurs (øverst på lista over aktiviteter), så Introkurs (øverst på lista over kurs).

### Medlemskontingent 2018

**Barn** (t.o.m. det året man fyller 6 år)       — kr 100,-
**Junior** (t.o.m. det året man fyller 18 år) — kr 125,-
**Senior** (f.o.m. det året man fyller 19 år) — kr 275,-

Kontingenten gjelder én sesong, uansett når man melder seg inn i løpet av året. Beløpet dekker også premien til If klubbforsikring.  Det var en økning i fjor fra 25 kr til 100 kr for barn, etter endring av Norges idrettsforbund sin lovnorm, der minste tillatte medlemskontingent er kr 100,-.

**Betaling av medlemskap i MinIdrett**
Etter at du har gjennomført og bestått introkurset kan du registrere og betale medlemskapet ditt i medlemssystemet vårt, [**MinIdrett**](http://minidrett.no/). Informasjon om hvordan du bruker dette systemet finner du i [**dette pdf-dokumentet**](http://nrpk.no/wp-content/uploads/2016/08/Les-her-om-hvordan-du-aktiverer-medlemskap-og-betaler-medlemskontingent.pdf).

Fakturering i Min Idrett for medlemmer gjøres normalt i løpet av april, etter at årsmøtet har vedtatt satser for medlemskontingent.

### Vært medlem tidligere?

Medlemmer som tidligere har meldt seg ut av klubben, eller som ikke har betalt medlamsavgiften slik at medlemsstatusen er slettet, *men* som ønsker å melde seg inn igjen i NRPK, må:

* Ha gjennomført introkurs tidligere. Hvis ikke – ansees dere som helt nye medlemmer, og må melde dere inn etter de kriteriene som gjelder for disse.
* Ha betalt eventuelt utestående medlemsavgift til NRPK – eller andre idrettslag.
* Etter 2 år uten betaling av medlemsskapet er man i følge lovnormen å betrakte som nytt medlem.  Introkurs må gjennomføres på nytt for å bli medlem igjen.
* Sende en mail til [medlem@nrpk.no](mailto:medlem@nrpk.no) med informasjon om hva henvendelsen gjelder, innholdende fullt navn, adresse, når introkurset ble tatt (årstall og ca tidspunkt eller kursreferanse).