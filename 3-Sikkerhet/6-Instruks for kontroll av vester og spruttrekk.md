# Instruks for kontroll av vester og spruttrekk

Publisert: 14. august 2016 / Sist oppdatert: 25. august 2016
Kontrollen skal sikre at utstyret fungerer etter hensikten og for å reparere eller kassere utstyr som er slitt eller dårlig.
Instruksen gjelder Materialforvalter, som har hovedansvaret for utstyret, og de som har roller i dette arbeidet.
Materialforvalter har ansvar for å se til at det er samsvar i mellom instruks og praksis.
Det skal utføres kontroll minst ved hver sesongstart.
Den som utfører kontroll skal ha opplæring av materialforvalter eller en person materialforvalter har godkjent for opplæring.
Det skal kontrolleres at stropper og sømmer er hele. Det skal også kontrolleres at flyteelementer i vestene er i orden. Barnevester og redningsvester skal ha skrittstropp som fungerer. Spruttrekk skal være hele og ha utløsningsstropp/hempe. Det skal notere noteres skriftlig i materialforvalters perm på klubben hvilket arbeid som er gjort.
Materialforvalter, eller en han har utpekt, tar endelig avgjørelse om utstyr skal kasseres.