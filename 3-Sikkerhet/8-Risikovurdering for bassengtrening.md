# Risikovurdering for bassengtrening

Publisert: 14. august 2016 / Sist oppdatert: 25. august 2016

Endringer i sikkerhetsregler for bassengtrening skal vurderes i lyset av denne risikovurderingen.
Risikovurdering for bassengtrening – nrpk 2014-06-08

| **Faremoment**                                      | **Risiko-faktor** | **Tiltak**                                                   |
| --------------------------------------------------- | ----------------- | ------------------------------------------------------------ |
| Drukning – fall / slag                              | Middels           | Regler bassengtrening – utvise forsiktighetAlltid vakt til stede |
| Drukning – spruttrekk                               | Høy               | Regler bassengtrening – om spruttrekkAlltid vakt til stede   |
| Drukning – overse bevisstløs person                 | Lav               | Regler bassengtrening – om å være årvåkenKrav om minst to personer i badet til alle tidspunktMer enn en bassengvakt |
| Drukning – panikk ved velt                          | Middels           | Regler bassengtrening – om spruttrekkMer enn en bassengvakt  |
| Drukning – utilstrekkelig kompetanse til livredning | Lav               | Krav om livredningskurs for bassengvakter                    |
| Drukning / skade når mange barn er til stede        | Middels           | Regler bassengtrening – vakter skal vurdere behov for «på-kanten»-vak ut i fra deltagerne som deltarMer enn en bassengvakt |
| Skader pga fall                                     | Middels           | Regler bassengtrening – utvise forsiktighet                  |
| Svømmende skades av åre                             | Middels           | Regler bassengtrening – utviske forsiktighetGjøre nye vurderinger ifm ballspillAlltid vakt til stede |
| Brann                                               | Lav               | Regler bassengtrening – vakt skal kjenne nødutganger / brannslukkingsutstyrRelger bassengtrening – vakt skal vite hvor personer er i byggetAlltid vakt til stede |
