# Sikkerhet

![img](http://nrpk.no/wp-content/uploads/2016/08/sikkerhet.png)

Publisert: 6. august 2016 / Sist oppdatert: 17. mai 2018

Fra klubbens HMS-system:

* Bruk vest. Alle som bruker klubbens båter skal ha vest på – det er ikke nok å ha den med i båten. Små barn og de som ikke kan svømme må ha redningsvest
* Hold deg langs land, spesielt når det er kaldt i vannet.
* Dersom du velter, svøm til land. IKKE bruk opp kreftene på å få med deg båten.
* Ha med deg tøyskift og håndkle. Oppbevares i vanntett sekk eller i klubben mens du er på vannet.

## Spruttrekk

Spruttrekk tetter rundt cockpit og padler slik at vann kun i liten grad trenger ned i kajakken. Dette kan være fint for å holde seg tørr, ved padling i bølger på sjøen, eller ved eskimorulle. Vi anbefaler ikke bruk av spruttrekk før du har prøvd hvordan du tar deg ut av kajakk med spruttrekk etter velt. Husk at alvorlige ulykker ofte skjer som en følge av en uheldig kombinasjon av omstendigheter. For en padler kan det være at han velter med spruttrekk når det er dårlig sikt (halvmørkt), utløsningsstroppen ligger desverre vendt inn i båten, og han har hansker på.

Som hovedregler ved bruk av spruttrekk-

* Vend alltid utløserstropp ut
* Øv med det utstyret du skal bruke.

Vi anbefaler derfor at du øver før du bruker spruttrekk:

* Prøv å velt med spruttrekk mens en person står ved siden av kajakken, klar til å hjelpe. (En hjelper i kajakk vil i enkelte situasjoner bruke en del lengre tid før han kan bistå.)
* Du husker ikke alltid å vende stroppen ut (hvis du er som meg) – prøv velt hvor du frigjør deg uten å bruke stroppen.
* Hvis du skal bruke hansker – prøv med dette også.

De som ønsker å øve med andre klubbmedlemmer kan gjøre dette under onsdagsbading eller bassengtreninger.
Så… ut og øv:)

## Bleier for barn

Barn med vanlig bleie kan bli liggende med nese og munn under vann fordi den vanlige bleien har så stor oppdrift at redningsvesten ikke klarer å rette opp barnet. Skift derfor ut bleien med en egen badebleie før du tar med deg bleiebarn ut i båt, eller la dem sitte i båten uten bleie. Badebleier finnes i klubbhuset.  

## Sikkerhetsanbefalinger

Vi regner jo med at dere foreldre passer godt på barna når de er med. Men for ordens skyld skriver vi:

* Bruk redningsvest med krage på barn som ikke kan svømme, eller som ikke er helt fortrolig med vannet.
* Pass på at glidelås er trukket opp, og at belte er strammet.

Og ikke minst:

* *Bruk egen badebleie på bleiebarn!* Barn med vanlig bleie kan bli liggende med nese og munn under vann fordi den vanlige bleien flyter og motvirker vesten! Skift derfor ut bleien med en egen badebleie før du tar med deg bieiebarn ut i båt, eller la dem sitte i båten uten bleie. Badebleier finnes i klubbhuset.

**Når det er kaldt i vannet**

Er velt om sommeren er en ting. Noe helt annet er en velt når det er kaldt i vannet! Vi har derfor satt opp følgende anbefalinger for dere som liker å tøye sesongen i begge ender:

* *Ta ikke med de minste når det er kaldt i vannet.*
* Hold deg langs land. Dersom du velter, svøm til land. IKKE bruk opp kreftene på å få med deg båt og åre.
* Hvis det ligger an til å bli mye vind og bølger: Velg en stødigere båttype og / eller bruk tørrdrakt. Eller: Gjør noe annet den dagen.
* Ha gjerne med deg tøyskift og håndkle. Oppbevares i vanntett sekk eller på klubben mens du er på vannet.
* *Les mer om nøyaktig hvor farlig kaldt vann kan være i artikkelen «Sikkerhet ved padling» På vår nettside!*

## Kommer du løs hvis du velter?

Noen sier at man alltid kommer seg ut hvis man velter uten spruttrekk. Et har hendelser har vist at det noen ganger likevel kan være vanskelig å komme ut ved velt

* Klær eller tørrdrakt kan bidra til at det blir vanskeligere å komme ut av en trang cockpit
* Skolisser eller snorer kan henge seg fast
* Sko – myke padlesko er å anbefale.
* Lårstøtte, som brukes på havkajakker, elvekajakker og polokajakker, er normalt ikke et problem, men man bør øve med dette hvis båten er trang
* Hvis kajakken er trang vil faktorer som er nevnt over bli spesielt viktig.
* Det skal alltid øves med spruttrekk før man velger å bruke det. Dette må skje under trygge forhold.

Vi vil ikke skremme medlemmer fra å padle, men, gjør en vurdering før du går ned i en trang kajakk.

Hvis du bruker båter med romslig cockpit er dette ikke noe problem.