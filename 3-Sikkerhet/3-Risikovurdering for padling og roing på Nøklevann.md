# Risikovurdering for padling og roing på Nøklevann

Publisert: 14. august 2016 / Sist oppdatert: 15. mars 2017

FAREMOMENTER, RISIKO OG TILTAK
Risikovurderingen under er en oppdatering av eksisterende vurdering, og er en kladd under arbeid.

| **Faremoment**                                      | **Risiko**    | **Tiltak [Ytterligere mulig tiltak]**                        |
| --------------------------------------------------- | ------------- | ------------------------------------------------------------ |
| Velt i kaldt vann – drukning                        | Stor          | Vestpåbud, regel om å padle nært land, regel om tøyskift, påkledning ved kaldt vann, introkurs |
| Velt i kaldt vann – kulde etter ilandstigning       | Liten         | Som ved drukning.                                            |
| Drukning / farlig situasjon ved velt med spruttrekk | Middels / høy | INFO PÅ INTROKURS OG HJEMMESIDE – SE «Utilstrekkelig kompetanse og kunnskap» nedenfor |
| Feilmontert redningsvest på barn                    | Liten         | INTROKURS; Våkne nøkkelvakter; badebleier.                   |
| Velt – dårlig / manglende svømmeferdighet           | Middels       | Vestpåbud, obligatorisk velt i Introkurs. Informasjon på hjemmeside om redningsvest for barn og de som har redusert evne til å svømme. |
| Panikk ved velt                                     | Middels       | Vestpåbud, obligatorisk velt i Introkurs                     |
| Illebefinnende på vannet                            | Middels       | Vestpåbud, førstehjelputstyr. ANBEFAL Å VURDERE REDNINGSVEST HVIS ØKT RISIKO PGA SYKDOM ETC. |
| Barn savnet                                         | Middels       | Aktive og observante nøkkelvakter; MEDLEMSREGLER; «VEST PÅ BARN FØRST» I INTROKURS; LET VED VANN FØRST HVIS DET OPPSTÅR; DYKKERMASKE I HYTTA |
| Alkohol / rusmidler                                 | Stor          | Forbud mot rusmidler ved padling; NØKKELVAKTER AVVISER BERUSEDE PERSONER. |
| Vind, vær og dårlig sikt                            | Middels       | Dårlig vær er problem når det oppstår sammen med andre risikofaktorer – se andre punkter i denne tabellen. LYKT I HYTTA FOR MØRKE KVELDER |
| Skadet utstyr                                       | Middels       | Jevnlig kontroll av utstyr. Utstyr blir tatt ut av bruk når feil oppdages. |
| Skade ved inn- og utstigning båt                    | Stor          | Tilsyn av brygge i åpningstid, vestpåbud, Introkurs, FØRSTEHJELPSBEREDSKAP |
| Utilstrekkelig kompetanse og kunnskap               | Middels       | Introkurs, informasjon hjemmeside, klubbmedlemmer kan helt eller delvis få dekket kurs til livredning, førstehjelp, sikkerhet og padling. |
| UHELL PGA MANGE PERSONER PÅ BRYGGA                  | Middels       | SETT OPP SKILT PÅ BRYGGA; AKTIVE NØKKELVAKTER TAR AFFÆRE; UNDERVANNSKIKKERT; UNDERVANNSLYKT; DYKKERMASKE I HYTTA |
| Skade ved opphold på klubben, fall                  | Middels       | TILGJENGELIGHET AV UTSTYR; ORDEN PÅ KLUBBEN; MATERIALANSVARLIG VURDERER SIKKERHET PÅ ANLEGG; FØRSTEHJELPSBEREDSKAP |
| Brann                                               | Lav           | ELSIKKERHET PÅ KLUBBEN; BRANNSLUKKER                         |

 