# Organisering

Klubben er organisert med et styre, valgkomite, kontrollkomite og to revisorer. Ulike komiteer og utvalg oppnevnes etter behov av årsmøte eller styre. Presentasjonen av det sittende styret finner du i menyen under ‘Om klubben’.

# Klubbtilbud

NRPK er i dag et populært tilbud til familier og enkeltpersoner i nærmiljøet. I mars 2014 har klubben ca. 2800 medlemmer. Flertallet av klubbens medlemmer benytter seg av tilbudet til rekreasjon.

Grunntilbudet til alle medlemmene er muligheten til å benytte klubbens materiell på Nøklevann innenfor sesongens åpningstider. Man kan da få hjelp til å ta materiellet ut og inn samt få råd og hjelp av vaktene ved valg av materiell.

På flotte sommerdager med stor etterspørsel kan utlånstiden bli begrenset til tre timer. Vaktene styrer dette.

Klubben disponerer båter: kanoer, kajakker og robåter+ SUP-brett. De aller fleste båtene kan brukes av alle, også nybegynnere. Vi har i tillegg noen mer avanserte båter for viderekomne. Klubben har årer og vester i godt utvalg.
<!--  -->
For informasjon om kurs, se under menyvalget Kurs. Vi har for tiden ikke tilbud til dem som ønsker omfattende instruksjon og fellestrening i padling. Vi har heller ikke tilbud til roere som ønsker omfattende instruksjon og fellestrening. Grunnleggende instruksjon kan bli gitt ved henvendelse til ro-ansvarlig.

# Formål og historikk

Nøklevann ro- og padleklubb (NRPK) ble stiftet på Rustadsaga i februar 1988 etter et initiativ fra ledende personer innen ro- og padlemiljøet i Oslo. Initiativet ble tatt 21. desember 1987 og det er denne datoen som er blitt stående som stiftelsesdatoen. Hensikten med opprettelsen var fra stifternes side å legge forholdene til rette for roing og padling på Nøklevann.

## Klubbens formål er tredelt:

*   Å tilrettelegge for roing og padling på Nøklevann, også for funksjonshemmede.
*   Å drive idrett organisert i Norges Idrettsforbund.
*   Å jobbe aktivt for å bevare Nøklevann som et friluftsområde til nytte for lokalbefolkningen i omegnen.

Klubbens lov bygger på lovnormen til Norges Idrettsforbund. Klubben er medlem av Norges Idrettsforbund gjennom Oslo Idrettskrets og er også medlem i de to særforbundene Norges Roforbund og Norges Padleforbund.

