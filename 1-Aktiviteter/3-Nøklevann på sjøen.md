## Nøklevann på sjøen

### Mosseveien 169 B

De som følger med på klubbens hjemmeside har sikkert registrert at noen kurs og gruppeturer har start fra Mosseveien. Her kommer en nærmere beskrivelse av klubbens utvidelse med litt historikk.

Ett koselig lite rødmalt hus vei vannkanten. Tett grønn vegetasjon som skjermer godt fra Mosseveien. Det er åpent mot fjorden med utsikt over småbåthavnet på Padda og Ulvøya. Kajakkstativ i hagen med fine havkajakker. Hva mer kan enn som er glad i sjøen og padling ønske seg?

Prosjektet startet noen år siden da vi fikk forespørsel fra Ormsund Roklubb vedrørende en gammelt båthus på Malmøya. Spørsmålet var om NRPK kunne tenke seg å dele dette med Ormsund Roklubb. Dette sa vi i utgangspunktet ja til og sendte en felles søknad til eieren, Oslo kommune, men uten at vi kom i mål.

Når Vannverket begynte med restaureringsprosjektet på dammen i Nøklevann tenkte vi at det ville være fint om vi kunne tilby klubbens medlemmer muligheten til å padle på sjøen.  Gunvor Brekken gjorde en kjempeinnsats som resulterte i at klubben fikk leie et lite hus langs Mosseveien av kommunen. Huset ligger rett ved vannkanten like i nærheten av Ulvøya.

Etter en del dugnadsarbeid er hagen og området er kommet i orden. Klubben har satt opp 2 kajakkstativ med 14 havkajakker type Boral Baffin P1, P2 og P3 og to SUP-brett. Der er det også padlevester, årer, spruttrekk, lensepumper, svamper og annet.

Alle klubbens medlemmer som har «Våttkort grunnkurs hav», kan bruke kajakkene som er plassert der. Kravet om Våttkort-kurs er absolutt og av sikkerhetsmessig grunner (se HMS).

Nedenfor er det lagt ut regler og informasjon om bruk av klubben på Mosseveien, samt et bookingsystem for bestilling av båter.

#### Booking av båter

Gjøres her på nettsiden: Registrer deg som bruker om du ikke har gjort det, og velg «Kajakkbooking».  Velg ønsket båt, tast inn nødvendige opplysninger, og en epost-bekreftelse på valgt båt og tidsrom kommer i retur. Vi har forbedringer på løsningen underveis.

#### Adkomst

Kart
Direktelenke til kart: https://goo.gl/maps/2tKQpL3jtb62

Adkomst med bil
For de som kommer med bil så er det gateparkering i Skrenten, som er veien mellom Shell på Nedre Bekkelaget og Padda.
https://goo.gl/maps/VYW73o4izxr

Det kan også parkeres på denne offentlige parkeringen rett sør for Shellstasjonen: https://goo.gl/maps/68wiG2YS61T2

Adkomst med buss
Hvis du reiser kollektivt så er det busstopp like i nærheten på Mosseveien. Stoppet heter Ulvøybrua.
https://goo.gl/maps/9zrASf3n1nR2

Adkomst med sykkel
Det er sykkelvei som går rett forbi porten til huset i Mosseveien. Sykler kan bæres ned trappen ned til området der vi har båtene.

#### Låsing av hus

Nøkkelboks med nøkkel er plassert nede til venstre ved inngangsdøra. Plastdekselet vippes ned, det tastes en 4-sifret kode, og lokket kan åpnet. For lukking tastes koden på nytt, og lokket og plastdekselet lukkes.

OBS! Dersom en uforvarende trykker på feil tast, blir denne en del av koden, og lokket låses ikke opp. For å “nullstille”, dra knappen i midten nederst nedover, og tast koden på nytt. 

#### Låsing av kajakker

Kajakkene er låst med wirelås med kode.

Vri til riktig kode og lås opp.
Etter at kajakken er løftet ut, lås igjen låsen og vri om på koden.
Når kajakken låses, pass på at wiren er en ekstra gang rundt stativt, slik at wireslyngen blir så stram at slyngen ikke kan tas av kajakken uten å låse opp låsen.

#### Registrering i loggperm

Det ligger loggperm i huset. Lån av kajakk skal registreres i permen. Notér eventuelle avvik, f.eks. feil på ror, lekkasjer eller liknende.

#### Utsetting og opptak av båter

Kajakken kan dras halvveis over kanten der trebjelken ligger, til høyre for badetrappa, og tilsvarende opp igjen. Utsetting og å ta kajakken opp igjen kan dermed håndteres av en person, selv om det er en fordel å være to.  Det er nå også etabert en kajakksklie, se bilde.

#### Renhold av båter

Sand og lignende vaskes med svamp. Husk å vaske lasterom og sitteåpning. Bøtter og svamper finnes i huset. Dras det mye skitt inn i huset, setter vi pris på om vaskekost/feiekost brukes innendørs.

#### Tørking av utstyr

Vått utstyr henges på badet der det er sluk. Utstyr som ikke er vått henges i hovedrommet.

#### Padlevett/sikkerhet/HMS

Padlevest skal brukes. Gjør vesten til en selvfølgelig del av bekledningen.
Vær synlig, men ikke stol på at du blir sett. Bruk farger som synes. Vær likevel oppmerksom og opptre defensivt. Det nytter lite å ha rett om man ikke blir sett.
Vær bevisst egne ferdigheter. Bruk padlestigen. Denne gir enkle regler for hva man bør tillate seg på et gitt nivå. Likevel, du må selv vite hva akkurat du kan mestre. Si alltid i fra om du nærmer deg en grense.
Les værmeldingen. Respekter vær og farvann. Man er utsatt i en kajakk, og man blir liten når det blåser opp. Kulde og motvind kan tappe krefter. Værmeldingen stemmer ikke alltid, men den er et godt utgangspunkt.
Kle deg etter vanntemperaturen. Spør deg selv hva konsekvensen blir om du havner i vannet eller om du må padle med våte klær. Kle deg etter det.
Vær godt rustet, selv på korte turer. Ha alltid med redningsutstyr som pumpe og årepose. Ta med deg litt ekstra klær og mat i en vanntett pose. Resten avhenger av turen du skal på.
Informer andre om turplanene. Gjør det til en vane å fortelle andre hvor du drar og hvilken tidsramme du har. Unngå deres bekymring ved å holde deg til planen.
Vær sosial. Hold oversikt og pass på alle. Det er tryggest å padle sammen med andre. Sjekk regelmessig at alle er med. Se etter tegn til tretthet eller nedkjøling. Si selv i fra om du blir usikker, trett eller kald.
Hjelp til med å holde gruppen samlet. Hold deg nær nok gruppen til å kunne kommunisere. Vær lojal mot turleder og utnevnte hjelpere.
Vær et forbilde. Jo mer erfaring du har jo viktigere er det å være et godt eksempel. Det nytter lite med regler om ikke gode forbilder viser hvordan de følges i praksis.
Oslo Kajakklubb har sammen med Padleforbundet utarbeidet en HMS-håndbok om sikkerhet for kajakkpadling på sjøen. Denne er tilgjengelig her.

#### Kart som viser toaletter på øyene

@todo