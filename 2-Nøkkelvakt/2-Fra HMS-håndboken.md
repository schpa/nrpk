# Fra HMS-håndboken

Nøkkelvaktene har noen plikter i henhold til klubbens internkontrollrutiner. Under følger det som gjelder for nøkkelvakter i fra «HMS-håndbok»:

Noen av punktene under gjelder hovedsakelig når nøkkelinnehaveren er i rollen som nøkkelvakt. Likevel, som nøkkelinnehaver forventes det at medlemmet yter ekstra hjelp overfor andre medlemmer, også når man ikke er på vakt:

*   Følg Vaktinstruks, Regler utenom vakt, og andre regler som er aktuelle.
*   Holde seg oppdatert mht. klubbens utstyr.
*   Delta regelmessig på nøkkelvaktmøter og eventuelle kurs.
*   Være beredt på å assistere i nødssituasjoner. Dette betyr ikke at vi anser at nøkkelvaktene er “badevakter”. Men, som vakt på anlegget, vil noen forvente at nøkkelvakten tar ledelse i en dramatisk situasjon.
*   Ta obligatoriske og frivillige kurs i klubbregi. Ta gjerne livredningskurs, førstehjelpskurs, kurs i våttkortsystemet og andre relevante kurs.
