# Medlemsfordeler (klubbavtaler)

Publisert: 11. september 2017 / Sist oppdatert: 17. mai 2018

[**West systems på Alnabru**](https://www.westsystem.no/m/279)
Medlemmer får 15 % på alt padleutstyr.  Dette gjelder ikke varer som allerede er på tilbud eller vår faste lavpris tørrdrakt (Typhoon Hypercurve).  På kajakker gis det ikke noen fast rabatt.  Medlemskort må framvises.

Til dere som klubb:
Her kan vi i noen tilfeller tilby noe mere enn 15 %, men det komme an på hva slags kjøp som skal gjøres.  Det er også mulighet til å få rabatter på ulike kajakker. Men dette kommer an på antall/mengde.

[**Bull Ski & kajakk**](https://bull-ski-kajakk.no/)
Avtale fra september 2017 til september 2019
Fast 20% rabatt på alle produkter i butikken. Unntakene er el-artikler (10%) og Epic/Nelo kajakker.
30 % rabatt på Bull-kolleksjonen (Buffer, bag, sekk mm)

**Milslukern**
Avtale kommer

**Eian fritid**
Gir ikke noen fast rabatt på Valley kajakker, men gir 10% klubbrabatt på annet utstyr, blant annet årer fra Werner.