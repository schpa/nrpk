# Lov for Nøklevann ro- og padleklubb

Vedtatt på NRPKs årsmøte 10. november 2004. Godkjent av Oslo Idrettskrets 25. januar 2005. Siste endringer vedtatt på NRPKs årsmøtet 7. mars 2012, godkjent av Oslo Idrettskrets 22. mai 2012.

## I. INNLEDENDE BESTEMMELSER

### § 1 Formål

Idrettslagets formål er:

1. å tilrettelegge for roing og padling på Nøklevann, også for funksjonshemmede.
2. å drive idrett organisert i Norges Idrettsforbund og olympiske og paralympiske komité (NIF)
3. å jobbe aktivt for å bevare Nøklevann som et friluftsområde til nytte for lokalbefolkningen i omegnen.

Arbeidet skal preges av frivillighet, demokrati, lojalitet og likeverd. All idrettslig aktivitet skal bygge på grunnverdier som idrettsglede, fellesskap, helse og ærlighet.

### § 2 Organisasjon

1. Idrettslaget er selveiende og frittstående med utelukkende personlige medlemmer. 
1. Idrettslaget er medlem av de(t) særforbund som lagets årsmøte bestemmer. Idrettslaget er pr. 31.12.03 medlem av Norges Roforbund og Norges Padleforbund.
1. Idrettslaget er medlem av NIF gjennom Oslo Idrettskrets, hører hjemme i Oslo kommune, og er medlem av Oslo idrettsråd. 
1. Idrettslaget skal følge NIFs og tilsluttede organisasjonsledds regelverk og vedtak. NIFs lov gjelder for idrettslaget uavhengig av hva som måtte stå i idrettslagets egen lov.

### § 3 Medlemmer

1. Alle som aksepterer idrettslagets og overordnede organisasjonsledds regelverk og vedtak kan bli tatt opp som medlem. En søker kan ikke tas opp som medlem uten å ha gjort opp de økonomiske forpliktelsene til idrettslaget og andre organisasjonsledd i NIF.
1. Medlemskap kan nektes i særlige tilfeller. Idrettslagets avgjørelse kan påklages til idrettskretsen. Klagefristen er tre uker etter at vedtaket er mottatt. Idrettskretsens avgjørelse kan påklages til Idrettsstyret innen tre uker etter at rekommandert melding er mottatt.
1. Medlemskap i idrettslaget er først gyldig og regnes fra den dag første kontingent er betalt.
1. Medlemmet plikter å overholde NIFs, tilsluttede organisasjonsledds og idrettslagets regelverk og vedtak.
1. Utmelding skal skje skriftlig og får virkning når den er mottatt.
1. Idrettslaget kan frata medlemskap fra medlem som etter purring ikke betaler fastsatt medlemskontingent. Medlem som skylder kontingent for to år taper automatisk sitt medlemskap i idrettslaget og skal strykes fra lagets medlemsliste. Medlem som har tapt sitt medlemskap kan ikke tas opp igjen før skyldig kontingent er betalt.
1. Idrettslaget skal føre medlemslister. 
1. Idrettslaget plikter å føre elektroniske medlemslister i idrettens nasjonale medlemsregister i tråd med forskrift gitt av Idrettsstyret.

### § 4 Medlemskontingent og avgifter

Medlemskontingenten fastsettes av årsmøtet. Andre avgifter/egenandeler kan kreves for deltakelse i idrettslagets aktivitetstilbud.

## II. TILLITSVALGTE OG ANSATTE

### § 5 Kjønnsfordeling

1. Ved valg/oppnevning av representanter til årsmøte/ting, samt medlemmer til styre, råd og utvalg m.v. i idrettslaget skal det velges personer fra begge kjønn.
1. Sammensetningen skal være forholdsmessig i forhold til kjønnsfordelingen i medlemsmassen, dog slik at det skal være minst to representanter fra hvert kjønn der det velges eller oppnevnes mer enn tre personer. Der det velges eller oppnevnes tre personer eller færre skal begge kjønn være representert. Ansattes representant teller ikke med ved beregningen av kjønnsfordelingen. 
1. Idrettskretsen kan, når det foreligger særlige forhold, gi dispensasjon fra denne bestemmelsen.
1. Idrettsstyret kan gi forskrift om nærmere vilkår for dispensasjon og konsekvensene av manglende oppfyllelse av bestemmelsen.

### § 6 Generelle regler om stemmerett, valgbarhet, forslagsrett m.v.

1. For å ha stemmerett og være valgbar må man være fylt 15 år, vært medlem av idrettslaget i minst én måned og ha oppfylt medlems-forpliktelsene. Ingen kan møte eller avgi stemme ved fullmakt. 
1. En arbeidstaker i et idrettslag har ikke stemmerett på idrettslagets ordinære eller ekstraordinære årsmøte. Dette gjelder ikke for spiller/utøver med kontrakt og medlemskap i laget. 
1. En person kan ikke samtidig inneha mer enn ett av følgende verv i idrettslaget: medlem av styre, valgkomité, kontrollkomité, lovutvalg, revisor. 
1. En person kan ikke ha tillitsverv knyttet til samme idrett i flere idrettslag som deltar i samme konkurranse. 
1. Engasjert revisor har talerett på ordinært og ekstraordinært årsmøte i saker som ligger innenfor sitt arbeidsområde. 
1. Representant fra overordnet organisasjonsledd har talerett på ordinært og ekstraordinært årsmøte i idrettslaget.
1. Med forslagsrett menes retten for medlem til å fremme forslag til idrettslagets årsmøte og til å fremme forslag under årsmøtet.

### § 7 Valgbarhet og representasjonsrett for arbeidstaker og oppdragstaker

1. En arbeidstaker i idrettslaget er ikke valgbar til verv i laget eller overordnede organisasjonsledd. Tillitsvalgt som får ansettelse i idrettslaget plikter å fratre tillitsvervet, og gjeninntrer når ansettelsesforholdet opphører. 
1. En arbeidstaker i idrettslaget kan ikke velges eller oppnevnes som representant til årsmøte/ting eller møte i overordnede organisasjonsledd.
1. Første og andre ledd gjelder ikke for arbeidstaker som er spiller/utøver med kontrakt og medlemskap i idrettslaget. 
1. Bestemmelsen får tilsvarende anvendelse på person som har oppdragsavtale som i omfang kan sammenlignes med et ansettelsesforhold med idrettslaget.
1. Denne bestemmelsen er ikke til hinder for at idrettslaget gir de ansatte rett til å utpeke et eller flere medlemmer blant de ansatte til idrettslagets styre.
1. Idrettskretsen kan, når det foreligger særlige forhold, gi dispensasjon.
1. Idrettsstyret kan gi forskrift om nærmere vilkår for dispensasjon.

### § 8 Valgbarhet og representasjonsrett for andre personer med tilknytning til idrettslaget

1. En person som har en avtale med idrettslaget som gir vedkommende en økonomisk interesse i driften av idrettslaget er ikke valgbar til verv innen idrettslaget eller overordnet ledd. Det samme gjelder styremedlem, ansatt i eller aksjonær med vesentlig innflytelse i en juridisk person med økonomiske interesse i driften av idrettslaget. Begrensningen gjelder ikke for styremedlem oppnevnt av idrettslaget. Tillitsvalgt som får en slik avtale, styreverv, ansettelse eller eierandel, plikter å fratre tillitsvervet, og gjeninntrer når forholdet opphører. 
1. Person som i henhold til første ledd ikke er valgbar, kan heller ikke velges eller oppnevnes som representant til årsmøte/ting eller møte i overordnede organisasjonsledd.
1. Idrettskretsen kan når det foreligger særlige forhold, gi dispensasjon.
1. Idrettsstyret kan gi forskrift om nærmere vilkår for dispensasjon.

### § 9 Inhabilitet

1. En tillitsvalgt, oppnevnt representant eller ansatt i idrettslaget er inhabil til å tilrettelegge grunnlaget for en avgjørelse eller til å treffe avgjørelse: 

  a.	når vedkommende selv er part i saken.

  b. 	når vedkommende er i slekt eller svogerskap med en part i opp- eller nedstigende linje eller i sidelinje så nær som søsken.

  c.	når vedkommende er eller har vært gift med eller er forlovet eller samboer med en part.

  d.	når vedkommende leder eller har ledende stilling i, eller er medlem av styret i et organisasjonsledd eller annen juridisk person som er part i saken.

1. Likeså er vedkommende inhabil når andre særegne forhold foreligger som er egnet til å svekke tilliten til vedkommendes upartiskhet; blant annet skal det legges vekt på om avgjørelsen i saken kan innebære særlig fordel, tap eller ulempe for vedkommende selv eller noen som vedkommende har nær personlig tilknytning til. Det skal også legges vekt på om inhabilitetsinnsigelse er reist av en part. 

1. Er en overordnet inhabil, kan avgjørelse i saken heller ikke treffes av direkte underordnet i idrettslaget.

1. Inhabilitetsreglene får ikke anvendelse dersom det er åpenbart at den tillitsvalgte, oppnevnte representanten eller ansattes tilknytning til saken eller partene ikke vil kunne påvirke vedkommendes standpunkt og idrettslige interesser ikke tilsier at vedkommende viker sete. 

1. Med part menes i denne bestemmelsen person, herunder juridisk person som en avgjørelse retter seg mot eller som saken ellers direkte gjelder. 

1. I styrer, komiteer og utvalg treffes avgjørelsen av organet selv, uten at vedkommende medlem deltar. Dersom det i en og samme sak oppstår spørsmål om inhabilitet for flere medlemmer, kan ingen av dem delta ved avgjørelsen av sin egen eller et annet medlems habilitet, med mindre organet ellers ikke ville være vedtaksført i spørsmålet. I sistnevnte tilfelle skal alle møtende medlemmer delta. Medlemmet skal i god tid si fra om forhold som gjør eller kan gjøre vedkommende inhabil. Før spørsmålet avgjøres, bør varamedlem eller annen stedfortreder innkalles til å møte og delta ved avgjørelsen dersom det kan gjøres uten vesentlig tidsspille eller kostnad.

1. I øvrige tilfeller avgjør vedkommende selv om vedkommende er inhabil. Dersom en part krever det og det kan gjøres uten vesentlig tidsspille, eller vedkommende ellers finner grunn til det, skal vedkommende selv forelegge spørsmålet for sin nærmeste overordnete til avgjørelse.

1. Bestemmelsen gjelder ikke på årsmøtet i idrettslaget.

### § 10 Vedtaksførhet, flertallskrav og protokoll

1. Når ikke annet er bestemt, er styrer, komiteer og utvalg i idrettslaget vedtaksføre når et flertall av medlemmene er til stede. Vedtak fattes med flertall av de avgitte stemmene. Ved stemmelikhet er møteleders stemme avgjørende. 
1. Vedtak kan fattes ved skriftlig saksbehandling  eller ved fjernmøte . Ved skriftlig saksbehandling sendes kopier av sakens dokumenter samtidig til alle medlemmer med forslag til vedtak. For gyldig vedtak kreves at flertallet av medlemmene gir sin tilslutning til det fremlagte forslaget, og til at dette treffes etter skriftlig saksbehandling. Ved fjernmøte skal alle møtedeltakerne kunne høre og kommunisere med hverandre.
1. Det skal føres protokoll fra styremøter.

### § 11 Tillitsvalgtes refusjon av utgifter og tapt arbeidsfortjeneste. Godtgjørelse.

Tillitsvalgt kan motta refusjon for nødvendige, faktiske utgifter inkludert tapt arbeidsfortjeneste, som påføres vedkommende i utførelsen av vervet. Tillitsvalgt kan motta en rimelig godtgjørelse for sitt arbeid. Refusjon for tapt arbeidsfortjeneste skal fremgå av vedtatt budsjett og regnskap. Styrehonorar skal fremkomme av årsberetningen.

## III. ØKONOMI

### § 12 Regnskap, revisjon, budsjett m.v.

1. Idrettslaget er regnskaps- og revisjonspliktig.
1. Idrettslaget skal engasjere revisor og velge kontrollkomité. Kontrollkomiteens oppgaver følger av NIFs lov § 2-12.
1. Bankkonti skal være knyttet til idrettslaget og skal disponeres av to personer i fellesskap. Underslagforsikring skal være tegnet for dem som disponerer.
1. På årsmøtet skal det fastsettes et budsjett som inneholder alle hovedposter i resultatregnskapet. Regnskap og budsjett for idrettslag som er organisert med grupper/avdelinger, skal også omfatte regnskapene og budsjettene for gruppene/avdelingene, og skal følge oppsettet i Norsk Standard kontoplan. 
1. Budsjettet skal være realistisk, og resultatet skal ikke vise underskudd med mindre det dekkes av positiv egenkapital.
1. Idrettslaget kan ikke gi lån eller stille garantier for lån hvis ikke lånet eller garantien er sikret med betryggende pant eller annen betryggende sikkerhet. Sikkerheten for lån og garantier skal opplyses i note til årsoppgjøret. 
1. Disposisjoner av ekstraordinær karakter eller betydelig omfang i forhold til idrettslagets størrelse og virksomhet, herunder låneopptak, skal vedtas av årsmøtet. Årsmøtet bør vedta et særskilt fullmakts-reglement knyttet til slike disposisjoner.

## IV. ÅRSMØTE, STYRE, UTVALG MV.

### § 13 Årsmøtet

1. Årsmøtet er idrettslagets øverste myndighet, og avholdes hvert år i mars måned.
1. Årsmøtet innkalles av styret med minst fem ukers varsel direkte til medlemmene eventuelt på annen forsvarlig måte, herunder ved kunngjøring i pressen, eventuelt på idrettslagets internettside. Innkalling kan henvise til at saksdokumentene gjøres tilgjengelig på internett eller på annen forsvarlig måte. Forslag som skal behandles på årsmøtet må være sendt til styret senest tre uker før årsmøtet. Fullstendig sakliste og andre nødvendige saksdokumenter med forslag må være gjort tilgjengelig for medlemmene senest én uke før årsmøtet.
1. Ved innkalling i strid med bestemmelsen, avgjør årsmøtet hhv. under godkjenning av innkalling og godkjenning av saklisten, om årsmøtet er lovlig innkalt og om det er saker som ikke kan behandles. 
1. Alle idrettslagets medlemmer har adgang til årsmøtet. Årsmøtet kan invitere andre personer og/eller media til å være tilstede, eventuelt vedta at årsmøtet kun er åpent for medlemmer.
1. Årsmøtet er vedtaksført dersom det møter et antall stemmeberettigete medlemmer som minst tilsvarer antallet styremedlemmer iht. idrettslagets lov. Dersom årsmøtet ikke er vedtaksført, kan det innkalles til årsmøte på nytt uten krav til minimumsdeltakelse.
1. På årsmøtet kan ikke behandles forslag om endring i lov eller bestemmelser som ikke er oppført på utsendt/kunngjort sakliste. Andre saker kan behandles når 2/3 av de fremmøtte stemmeberettigete på årsmøtet vedtar det, ved godkjenning av saklisten.

### § 14 Ledelse av årsmøtet

Årsmøtet ledes av valgt(e) dirigent(er). Dirigenten(e) behøver ikke være medlem av laget.

### § 15 Årsmøtets oppgaver

1. Årsmøtet skal:
    i. Godkjenne de stemmeberettigede.
    ii. Godkjenne innkallingen, saksliste og forretningsorden.
    iii.  Velge dirigent(er), referent(er), samt to medlemmer til å underskrive protokollen.
    iv.  Behandle idrettslagets årsmelding, herunder eventuelle gruppe-årsmeldinger.
    v.  Behandle idrettslagets regnskap i revidert stand.
    vi.  Behandle forslag og saker.
    vii.  Behandle idrettslagets handlingsplan, dvs. en plan for klubbens hovedaktiviteter i det kommende året.
    viii.  Fastsette medlemskontingent.
    ix.  Vedta idrettslagets budsjett, med egne poster for godtgjørelser til styret, valgkomite og kontrollkomite.
    x.  Behandle lagets organisasjonsplan. Organisasjons­planen skal regulere idrettslagets interne organisering, og inneholder som et minimum en oversikt over tillitsvalgte i tillegg til styret og de andre lovfestede tillitsverv.
    xi.  Engasjere statsautorisert/registrert revisor til å revidere idrettslagets regnskap.
    xii.  Foreta følgende valg:
        a.  Leder og nestleder.
        b.  Kasserer, sekretær, fire styremedlemmer samt 1. og 2. varamedlem. Varamedlemmer innkalles til alle styremøter. Har tale- og forslagsrett i alle saker og stemmerett ved forfall.
        c.  Øvrige valg i henhold i årsmøte vedtatt organisasjonsplan.
        d.  Kontrollkomité med to medlemmer og to varamedlemmer. 
        e.  Representanter til ting og møter i de organisasjonsledd idrettslaget har representasjonsrett.
        f.  Valgkomite med leder og to medlemmer og ett varamedlem for neste årsmøte.
    xiii.  Fastsette honorar til styret, til kontrollkomité, til valgkomité og eventuelle andre komiteer. Honorarene fordeles internt mellom medlemmene i styret og komiteene.
2. Leder og nestleder velges enkeltvis. De øvrige medlemmer til styret velges samlet. Deretter velges varamedlemmene samlet, og ved skriftlig valg avgjøres rekkefølgen i forhold til stemmetall.
3. Valgkomiteen velges på fritt grunnlag, etter innstilling fra styret.

### § 16 Stemmegivning på årsmøtet

1. Med mindre annet er bestemt i denne lov, skal et vedtak, for å være gyldig, være truffet med alminnelig flertall av de avgitte stemmer. Ingen representant har mer enn én stemme. Ingen kan møte eller avgi stemme ved fullmakt. Blanke stemmer skal anses som ikke avgitt. 
1. Valg foregår skriftlig hvis det foreligger mer enn ett forslag eller det fremmes krav om det. Hvis det skal være skriftlige valg, kan bare foreslåtte kandidater føres opp på stemmeseddelen. Stemmesedler som er blanke, eller som inneholder ikke foreslåtte kandidater, eller ikke inneholder det antall det skal stemmes over, teller ikke, og stemmene anses som ikke avgitt. 
1. Når et valg foregår enkeltvis og ingen kandidat oppnår mer enn halvparten av de avgitte stemmer, foretas omvalg mellom de to kandidater som har oppnådd flest stemmer. Er det ved omvalg stemmelikhet, avgjøres valget ved loddtrekning. 
1. Når det ved valg skal velges flere ved en avstemning, må alle, for å anses valgt, ha mer enn halvparten av de avgitte stemmer. Dette gjelder ikke ved valg av vararepresentant. Hvis ikke tilstrekkelig mange kandidater har oppnådd dette i første omgang, anses de valgt som har fått mer enn halvparten av stemmene. Det foretas så omvalg mellom de øvrige kandidater og etter denne avstemning anses de valgt som har fått flest stemmer. Er det ved omvalg stemmelikhet, avgjøres valget ved loddtrekning. 
1. For å være gyldig må valg være gjennomført i henhold til §§ 5 til 8.

### § 17 Ekstraordinært årsmøte

1. Ekstraordinært årsmøte i idrettslaget innkalles av idrettslagets styre med minst 14 dagers varsel etter:

    a.	Vedtak av årsmøtet i idrettslaget.

    b.	Vedtak av styret i idrettslaget.

    c.	Vedtak av styret i overordnet organisasjonsledd.

    d.	Skriftlig krav fra 1/3 av idrettslagets stemmeberettigete medlemmer.

1. Ekstraordinært årsmøte innkalles direkte til medlemmene eventuelt på annen forsvarlig måte, herunder ved kunngjøring i pressen, eventuelt på idrettslagets internettside. Innkalling kan henvise til at saksdokumentene gjøres tilgjengelig på internett eller på annen forsvarlig måte.

1. Ekstraordinært årsmøte er vedtaksført dersom det møter et antall stemmeberettigete medlemmer som minst tilsvarer antallet styremedlemmer iht. idrettslagets lov. Dersom det ekstraordinære årsmøtet ikke er vedtaksført, kan det innkalles til årsmøte på nytt uten krav til minimumsdeltakelse.

1. Ekstraordinært årsmøte i idrettslaget skal bare behandle de saker som er angitt i vedtaket eller i kravet om innkalling av årsmøtet. Sakliste og nødvendige saksdokumenter skal følge innkallingen.

### § 18 Idrettslagets styre

1. Laget ledes og forpliktes av styret, som er lagets høyeste myndighet mellom årsmøtene.

1. Styret skal:

    a.  Iverksette årsmøtets og overordnede organisasjonsledds-regelverk og vedtak, herunder gjennomføring av tiltak slik det er beskrevet i handlingsplanen.

    b.  Påse at idrettslagets midler brukes og forvaltes på en forsiktig måte i samsvar med de vedtak som er fattet på årsmøtet eller i overordnet organisasjonsledd, og sørge for at idrettslaget har en tilfredsstillende organisering av regnskaps- og budsjett-funksjonen samt en forsvarlig økonomistyring.

    c.  Oppnevne etter behov komiteer/utvalg/personer for spesielle oppgaver og utarbeide mandat/instruks for disse.

    d.  Representere laget utad.

    e.  Oppnevne ansvarlig for politiattest-ordningen.

    f.  Rapportere om sin virksomhet til årsmøtet i årsmeldingen.

1. Styret skal holde møte når lederen bestemmer det, eller et flertall av styremedlemmene forlanger det.

1. Styret er vedtaksført når et flertall av styrets medlemmer er til stede. Vedtak fattes med flertall av de avgitte stemmene. Ved stemmelikhet er møtelederens stemme avgjørende.

### § 19 Grupper/avdelinger/komiteer

1. Valgkomiteen skal legge frem innstilling på kandidater til alle tillitsverv som skal velges på årsmøtet, med unntak av valgkomité. Medlem av valgkomité som selv blir kandidat til verv, plikter å tre ut av valgkomiteen. 

1. Idrettslaget kan organiseres med grupper og/eller avdelinger for ulike idrettslige aktiviteter. Idrettslagets årsmøte bestemmer opprettelse av grupper/avdelinger, og hvordan disse skal organiseres og ledes. Dette vedtas i forbindelse med årlig behandling av idrettslagets organisasjonsplan, jf. § 15 pkt. 1, under pkt. x.

1. For grupper/avdelingers økonomiske forpliktelser hefter hele idrettslaget, og grupper/avdelinger kan ikke inngå avtaler eller representere idrettslaget utad uten styrets godkjennelse, jf. § 18.

## V. ØVRIGE BESTEMMELSER

### § 20 Alminnelige disiplinærforføyninger, sanksjoner etter kamp- og konkurranseregler, straffesaker og dopingsaker

For alminnelige disiplinær-forføyninger, sanksjoner etter kamp- og konkurranseregler, straffesaker og dopingsaker gjelder NIFs lov kapittel 11 og 12.

### § 21 Lovendring

1. Lovendring kan bare foretas på ordinært eller ekstraordinært årsmøte i idrettslaget etter å ha vært oppført på saklisten, og krever 2/3 flertall av de avgitte stemmer.
1. Lovendringer som følge av endringer i NIFs lov, trer i kraft straks. Lovendringer vedtatt av idrettslaget selv trer ikke i kraft før de er godkjent av idrettskretsen. Godkjenningen er begrenset til de bestemmelser som NIFs lov omfatter. 
1. I forbindelse med godkjenningen kan idrettskretsen pålegge nødvendig endring for å unngå motstrid med NIFs regelverk. 
1. Endringer i § 21 og § 22 kan ikke vedtas av idrettslaget selv.

### § 22 Oppløsning - Sammenslutning - Konkurs

1. Forslag om oppløsning av idrettslaget må først behandles på ordinært årsmøte. Blir oppløsning vedtatt med minst 2/3 flertall, innkalles ekstraordinært årsmøte tre måneder senere. For at oppløsning skal skje må vedtaket her gjentas med 2/3 flertall.
1. Sammenslutning med andre idrettslag anses ikke som oppløsning av laget. Vedtak om sammenslutning og nødvendige lovendringer i tilknytning til dette treffes i samsvar med bestemmelsene om lovendring, jf. § 21.
1. Ved oppløsning eller annet opphør av idrettslaget tilfaller lagets overskytende midler etter avvikling et formål godkjent av idrettskretsen. Underretning om at idrettslaget skal oppløses, skal sendes til idrettskretsen 14 dager før idrettslaget holder sitt ordinære årsmøte til behandling av saken.
1. Ved konkurs anses organisasjonsleddet som oppløst når konkurs er avsluttet og det mister således sitt medlemskap i NIF.


