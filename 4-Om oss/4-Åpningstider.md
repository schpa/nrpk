# Nøklevann

Sesongåpning 2018: Søndag 6. mai.

Følgende åpningstider gjelder f.o.m. 6. mai t.o.m. 2. september 2018:
Mandag: Stengt.
Tirsdag: 18.00-21.00
Onsdag: 18.00-21.00
Torsdag: 18.00-21.00
Fredag: Stengt.
Lørdag: 11.00-17.00
Søndag: 11.00-17.00

Fra og med 4. september 2018 har vi følgende åpningstider:
Onsdag: 18.00-20.00
Lørdag: 11.00-17.00
Søndag: 11.00-17.00

Sesongavslutning 2018: Søndag 7. oktober.

10. mai: Stengt.
17. mai: Stengt.

Pinsen: vanlig åpningstid.

 

Mosseveien 169B
Informasjon om åpningstidene for vår nye sjøbase ved Mosseveien 169B kommer.