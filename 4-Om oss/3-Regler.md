### Medlemsregler
1.  Ta på flytevest før man går ut på brygga. Barn og voksne som ikke kan svømme må bruke redningsvest med krage. På bleiebarn: Ta av bleien, eller bruk badebleie.
2.  Medlemskortet – utskrift eller på smartphone – skal vises uoppfordret til vakten hvis du skal låne båt. Uten gyldig kort kan du ikke regne med å få låne båt.
3.  Barn under 12 år må komme sammen med voksen (minst 18 år).
4.  Klubbens båter skal bare benyttes av medlemmene.
5.  Alle lån av båter skal føres i loggpermen. Skriv tydelig! Dersom det er flere personer i en båt, skal navnene på alle føres i loggen. Kryss av når du kommer tilbake.
6.  Medlemmer fra 6 t.o.m. 19 år må krysse av i feltet «Ungdom 6-19 år» i loggskjema.
7.  Utstyr medlemmer låner på klubbens anlegg skal kun brukes på Nøklevann.
8.  Ved overnatting må dette noteres i merknadsfeltet i loggen. Båten må leveres tilbake i åpningstiden dagen etter. Husk at klubben holder stengt mandager og fredager, og har ytterligere redusert åpningstid på høsten.
9.  Respekter åpningstidene. Du må være tilbake på brygga senest 15 minutter før stengetid.
10. Tørk godt ut av båtene etter bruk, og plasser båter og årer tilbake på riktig plass. Tørre vester henges i vestboden, fuktige vester henges ute til tørk på tørkesnorene.
11. Respekter klubbens retningslinjer for behandling av materiell.
12. Oppstår det skade på materiell, gi beskjed til vaktene. Det er viktig å melde fra slik at skadet materiell kan bli reparert så snart som mulig.
13. Plukk ikke vannliljene – la også andre få nyte synet.
14. Søppel tas med tilbake og kastes i søppelkasse ved badestranden. Ta gjerne med andres søppel også.
15. Hundelovens bestemmelser om båndtvang fra 1. april til 20. august (paragraf 6) gjelder klubbens område.
16. I følge NlFs retningslinjer skal barn og unge møte et røyk- og snusfritt miljø. Vi praktiserer derfor røykeforbud på klubbens område. Rus og padling skal ikke kombineres.