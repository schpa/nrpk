### Nøkkelvaktmøte

![img](http://nrpk.no/wp-content/uploads/2018/03/2017-05-02_1818-IMG_6775-1000x420.jpg)

Publisert: 8. juli 2017 / Sist oppdatert: 26. mars 2018

Nøkkelvaktmøte våren 2018 holdes torsdag 26. april kl. 18:00 på Rustadsaga Sportsstue. Det vil bli servert mat fra kl 17:30 til de som bestiller dette innen 22. april.

 

For de som ikke fikk deltatt på møtet høsten 2017 kan presentasjonen sees her: [Nøkkelvaktmøte høst 2017 presentasjon](http://nrpk.no/wp-content/uploads/2017/07/N%C3%B8kkelvaktm%C3%B8te-h%C3%B8st-2017-presentasjon.pdf)

