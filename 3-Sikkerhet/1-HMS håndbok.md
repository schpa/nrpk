# HMS-håndbok

## Innledning

Klubbens viktigste aktivitet er å tilby medlemmene å benytte klubbens båter på Nøklevann. Medlemmene låner båter og utstyr på eget initiativ, uten at dette er styrt av klubben. For denne typen bruk kan klubben bidra til sikkerhet i gjennom å tilby (a) sikkert utstyr, (b) klubbens krav og regler til medlemmene, og (c) med opplæring. Men, medlemmene må selv sørge for en adferd som fremmer egen sikkerhet. Fra klubbens siden legger vi vekt på at alle må kunne et minimum av sikkerhetsregler som bidrar til å redusere faren for uhell.

Der rutiner og regler fins i flere versjoner er det utgaven på klubbens hjemmesider som gjelder.

Nøkkelvakter representerer klubben på klubbens anlegg i åpningstiden. Klubbens utfordring her er at den enkelte nøkkelvakt i gjennomsnitt ikke arbeider så mange timer per år for klubben, men likevel har en ansvarsfull oppgave å utføre. En effektiv opplæring som når frem til alle nøkkelvakter er en kontinuerlig aktivitet som blir tillagt stor viktighet.

På turer som klubben arrangerer er forholdene annerledes. Her står klubben for direkte styring av aktivitetene. Klubben har en egen instruks for dette.

## Mål

* Unngå personskade i forbindelse med klubbens aktiviteter. Dette gjør vi med å prøve å unngå uønskede «*Hendelser*«, det vil si hendelser som under uheldige omstendigheter kan føre til personskade eller andre miljø- eller helsefarer.
* Bidra til at medlemmer er skikket til å ta vare på seg selv ved ferdsel på vannet
* Bevare naturen vi ferdes i, unngå forsøpling og unngå spredning av smitte i mellom vassdrag.

## Organisering

Årsmøtet velger styreleder, nestleder, og styret i Nøklevann Ro- og Padleklubb. Disse har det overordnede ansvaret for å planlegge, organisere, utføre og vedlikeholde internkontrollsystemet. Styret skal utnevne HMS-ansvarlig og personer til de andre rollene definert i dette dokumentet. Alle viktige roller skal ha en forankring i styret. Klubbens organisering skal være tilgjengelig på klubbens hjemmesider.

## Styret

Styret har i fellesskap det overordnede ansvar for aktiviteter i klubben, inkludert HMS. Styremedlemmene skal

* Ta aktivt del i HMS-arbeidet,
* Skal hver for seg forvisse seg om at alle saker blir håndtert på en forsvarlig måte med tanke på HMS.
* Se til at de som blir utpekt til oppgaver er kvalifiserte og oppfyller sine plikter.
* Se til at HMS-rutinene oppdateres når nødvendig for å nå oppsatte mål, og se til at endringer blir gjort i lys av sikkerhetsvurderinger.
* Styret skal vedta alle endringer av skriftlige HMS-rutiner. Innen avgrensede ansvarsområder kan styret delegere ansvar og myndighet for oppdatering av instrukser til ansvarlig og HMS-ansvarlig eller ansvarlig og leder. Med «ansvarlig» menes her en person som har en rolle i HMS-sammenheng, f.eks. material-ansvarlig. Delegering skal ikke skje innen områder som regulerer aktiviteten til et større antall medlemmer (som medlemsregler og nøkkelvaktinstruks). Styret skal bli orientert så fort som mulig etter endring og skal ha mulighet til å annullere endringer.

## Leder

Styret ledes av klubbens leder. Dette gir gir leder et overordnet ansvar for klubbens aktiviteter.

## HMS-ansvarlig

HMS-ansvarlig har det overordnede ansvar for å se til at klubben følger rutiner som på en best mulig måte sikrer at klubbens aktiviteter kan skje på en forsvarlig måte. Han har også ansvar for at klubbens rutiner også dekker vern av natur og miljø. De viktigste oppgavene er:

* Påse at klubbens arbeid skjer i henhold til dokumenterte rutiner, og at HMS-dokumentasjon er tilgjengelig.
* Fortløpende vurdere om klubbens rutiner er gode nok.
* Gjennomgang i forbindelse med «»*Melding om hendelser*«.
* Overordnet ansvar for formidling av HMS-informasjon til nøkkelvakter og medlemmer.
* En detaljert beskrivelse av den HMS-ansvarliges oppgaver er beskrevet i arbeidsinstruks: Arbeidsinstruks for HMS-ansvarlig i NRPK.

## IT-ansvarlig

1. Ha det tekniske ansvaret for klubbens hjemmesider.
1. Stå for system for lagring av dokumentasjon av klubbens rutiner, regler og opplæring.
1. Praktisere prinsippet om at klubbens originaler, regler og instrukser finnes åpent på hjemmesiden.
1. *Redaktør/IT-ansvarlig*: Se til at denne informasjonen er tilgjengelig for medlemmene. ==@todo: utdypning av dette punktet.==

## Kursansvarlig for introduksjonskurs

* Overordnet ansvar for at det blir gitt tilstrekkelig opplæring i sikkerhetsspørsmål.
* Sikre at det i alle introduksjonskurs deltar minst en kursholder som har aktivitetslederkurs i Norges padleforbunds Våttkortstige.
* Sørge for at det blir registrert hvem som går på kurs, og innholdet i kursene.
* At det blir informert om sikkerhetsutstyr på anlegget.

## Nøkkelvakter

Noen av punktene under gjelder hovedsakelig når nøkkelinnehaveren er i rollen som nøkkelvakt. Likevel, som nøkkelinnehaver forventes det at medlemmet yter ekstra hjelp ovenfor andre medlemmer, også når man ikke er på vakt:

* Følg Vaktinstruks, Regler utenom vakt, og andre regler som er aktuelle.
* Holde seg oppdatert mht klubbens utstyr.
* Delta regelmessig på nøkkelvaktmøter og eventuelle kurs.
* Være beredt på å assistere i nødssituasjoner. Dette betyr ikke at vi anser at nøkkelvaktene er «badevakter». Men, som vakt på anlegget, vil noen forvente at nøkkelvakten tar ledelse i en dramatisk situasjon.
* Ta obligatoriske og frivillige kurs i klubbregi. Ta gjerne livredningskurs, førstehjelpskurs, kurs i våttkortsystemet og andre relevante kurs.

##  Materialforvalter

* Overordnet ansvar for at båter og utstyr er tilfredsstillende når det gjelder sikkerhet og funksjon.
* Vedlikeholde / følge instrukser for å sikre dette. Ha en oversikt over klubbens sikkerhetsutstyr, og sørge for at det er i stand, merket og tilgjengelig.

 ## Turledere

Håndtering av sikkerhet skjer i henhold til instruks som er beskrevet i Instruks for turledere for klubbturer.

 ## Medlemmer

* Skal kunne Sikkerhetsreglene på Nøklevann
* Skal holde seg oppdatert gjennom NRPK sin nettside
* Skal lese informasjon som gis gjennom merking/oppslag på klubbanlegget
* Det anbefales jevnlig besøk på klubbens hjemmesider for å se oppdateringer og ny infformasjon.
* Skal kun bruke klubbens utstyr når medlemmet er skikket til det. F.eks. ikke kombinere rus og padling.

## Kartlegging av risiko

En kartlegging av de viktigste risikofaktorene for medlemmer som bruker klubbens materiell på Nøklevann er vist i Risikoanalyse for padling og roing på Nøklevann. Der er faremomentene vurdert, og tiltak for å unngå at uhell skal få uheldige konsekvenser er definert.

En mer utfyllende gjennomgang av padling og faren ved drukning er gitt i vår artikkel Sikkerhet ved padling, som finnes på vår hjemmeside.

For klubbturer eller andre aktiviteter utenfor Nøklevann må turlederen gjøre en vurdering av risiko, og på dette grunnlag gi instrukser om sikkerhetsutstyr og sikkerhetsfremmende adferd. Med klubbturer mener vi annonserte turer eller aktiviteter som er godkjent av styret og turleder er utpekt.

## Sikkerhethetsreglene på Nøklevann

Med en så stor medlemsmasse, hvor mange har forholdsvisk liten kunnskap om padling, velger vi å innprente et sett lettfattelige sikkerhetsregler:

1. Bruk vest. Alle som bruker klubbens båter skal ha vest på – det er ikke nok å ha den med i båten. Små barn og de som ikke kan svømme må ha redningsvest.
1. Hold deg langs land, spesielt når det er kaldt i vannet.
1. Dersom du velter, svøm til land. IKKE bruk opp kreftene på å få med deg båten.
1. Ha med deg tøyskift og håndkle. Oppbevares i vanntett sekk eller i klubben mens du er på vannet.

## Oppfølging av (HMS-) «Hendelser»

«Hendelser» er situasjoner som under uheldige omstendigheter kan føre til personskade eller andre miljø- eller helsefarer.

1. Nøkkelvakt har ansvar for at skjema for «*Melding omg hendelse*» blir fylt ut. Fortrinnsvis skal skjemaet fylles ut av den som observerer eller er involvert i en hendelse. Leder, HMS-ansvarlig eller et annet styremedlem varsles.
1. Når rapport blir mottatt av HMS-ansvarlig gis det tilbakemelding om at den er mottatt. Den blir arkivert og styret blir informert.
1. HMS-ansvarlig sørger for at rapport blir gjennomgått og fulgt opp. Eventuelle nødvendige opplysninger blir samlet inn.
1. Sak blir styrebehandlet, eventuelt behandlet av en gruppe i styret som har fått dette ansvaret. Konklusjon blir fattet. Det tas stilling til korrigerende tiltak for å redusere faren for tilsvarende uhell.
1. Oppfølging av korrigerende tiltak. Lagring av dokumentasjon. Tilbakemelding til involverte parter, eventuelt også publisering av informasjon på hjemmesidene.

Denne beskrivelsen er fokusert på hendelser som har ført til, eller kunne ha ført til, en farlig hendelse. Systemet kan også brukes til å gi tilbakemelding på utilstrekkelige sikkerhetsrutiner eller uheldig praksis i klubben.

Ved kriser som vil eller kan berøre personer sterkt, eller hvor media kan bli involvert, følges Beredskapsplan for Nøklevann ro- og padleklubb.

 

## **Andre HMS-dokumenter i NRPK**

* Instruks for nøkkelvakter- Vaktinstruks
* For nøkkelvakter – Regler utenom vakt
* Instruks for kontroll av NRPKs båter
* Instruks for kontroll av vester og spruttrekk
* Instruks for turledere for klubbturer
* Vår sikkerhetsinformasjon på vår hjemmeside – Sikkerhet ved padling
* Arbeidsinstruks for HMS-ansvarlig i NRPK
* Vaktrapport, inkludert Melding om hendelse
* Beredskapsplan for Nøklevann ro- og padleklubb.
* Risikofaktorer ved padling og roing på Nøklevann
* Sikkerhetsregler for bassengtrening

 

## **Aktuelle lover og forskrifter**

 

FOR 1996-12-06 nr 1127: Forskrift om systematisk helse-, miljø- og sikkerhetsarbeid i virksomheter (Internkontrollforskriften)

LOV 1976-06-11 nr 79: Lov om kontroll med produkter og forbrukertjenester (produktkontrolloven)

FOR 1995-05-08 nr 409: Forskrift om flyteutstyr om bord på fritidsfartøy