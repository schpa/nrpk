# Instruks for kontroll av NRPKs båter
Publisert: 14. august 2016 / Sist oppdatert: 25. august 2016
Hensikten med kontrollen er å avdekke feil og mangler som kan påvirke sikkerhet og funksjon for materiellet på et så tidlig tidspunkt som mulig – og helst før feilen inntreffer.
Denne instruksen gjelder for den som gjennomfører en planlagt kontroll av NRPKs båter. Alle båter skal kontrolleres ved sesongstart og ved halvgått sesong. Materialforvalter skal administrere kontrollen.
Den som utfører kontrollen skal ha blitt opplært av Materialforvalter, eller en person som Materialforvalter har godkjent for opplæring.
Kontrollen skal omfatte skrog, lasteluker, ror/senkekjøl, fotstøtter, dekksrigging, bærehåndtak, sete, åregafler og propper.
Båter med sikkerhetsmessige skader/mangler skal merkes tydelig ved å sette fast en lapp med tekst om at båten ikke skal brukes. Alternativt skal båten legges vekk slik at den ikke kan tas i bruk før den er reparert på en tilfredsstillende måte.
Dokumentasjon som støtter denne instruksen oppbevares i Materialforvalters perm på klubbens anlegg ved Nøklevann. Denne dokumentasjonen er:
Plan for testing av NRPKs båter
Kontrollskjema for NRPKs båter
Reparerte båter (en logg).
 