# Regler utenom vakt

*   Nøkkelvakten kan låne utstyr utenom klubbens åpningstider. Utlån loggføres.
*   Utstyret kan kun brukes på Nøklevann.
*   Nøkkelvakt kan også låne utstyr til familie og venner utenom åpningstiden. 

>   Merk
>
>   Dette regnes som private aktiviteter, og dekkes ikke av klubbens forsikring.
>   Nøkkelvakt må vurdere hvordan dette kan skje på en sikker måte.
>   Lånet skal avsluttes før klubben åpner.
>   Når du låner båter til ikke-medlemmer—noter også navn for ikke-medlemmene i utlånsloggen!

Det går å dele på nøkkelvakttjeneste i husstand. Dette forutsetter:

*   begge skal være registrert som nøkkelvakt, og
*   begge har et eget ansvar for seg holde seg oppdatert på klubbens regler og praksis. Begge skal delta på møter.

Når du ikke har vakt: Hvis du ser at klubben er underbemannet oppfordrer vi sterkt til at du trer inn som tilleggsvakt.