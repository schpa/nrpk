# Arbeidsinstruks for HMS-ansvarlig i NRPK

Publisert: 14. august 2016 / Sist oppdatert: 25. august 2016

HMS-ansvarlig har det overordnede ansvaret for å se til at klubbens har, og følger, rutiner som på en best mulig måte sikrer at medlemmene kan utøve sine aktiviteter i klubben på en måte som ikke medfører helsefare. HMS-ansvarlig har også ansvar for å se til at klubben ikke har aktiviteter som gir unødig belastning på natur og miljø.

Blandt oppgavene inngår oppfølging av rutiner:

* Hovedansvar for dokumentasjon av HMS-systemet.
* Fortløpende vurdere om klubbens rutiner er gode nok.
* Påse at det er overensstemmelse i mellom klubbens arbeid og klubbens skriftlige rutiner.
* Se til at skriftliger rutiner innen HMS er lett tilgjengelig på (1) klubbens anlegg, og (2) på klubbens nettsider.
* Gjennomgang av HMS-organisering minst en gang hvert år.
* Holde seg oppdatert innen lover og forskrifter, og hvordan de bør praktiseres.
* Vurdering av HMS-systemet i forbindelse med «Melding om hendelse».

Det inngår også et informasjonsansvar:

* Overordnet ansvar for formidling av HMS-informasjon til nøkkelvakter.
* Overordnet ansvar for formidling av HMS-informasjon til medlemmer.
* Påse at nøkkelvakter har nødvendig kontaktinformasjon til nødetater, klubbens leder, klubbens nestleder og HMS-ansvarlig.
* Motivere for økt kompetanse innen sikkerhet. Føre oppdatert liste over relevant kompetanse, samt hvilke kurs som nøkkelvakter og styremedlemmer har tatt.
